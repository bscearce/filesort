package com.scearce.filesort;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileTime;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

public class FileSort extends JFrame implements PropertyChangeListener {

	protected JTextField targetText = new JTextField();
	protected JTextField destText = new JTextField();
	protected JFileChooser fdia;
	
	private ImageIcon icon;
	private JButton sortButton;
	private JButton targetButton;
	private JButton destButton;
	private JButton cancelButton;
	private JProgressBar progressBar;
	private JComboBox<String> sortOption;
	private Task task;
	
	public FileSort()
	{
		initUI();
	}
	
	public static void main(String[] args)
	{
		EventQueue.invokeLater(() -> {
			FileSort fs = new FileSort();
			fs.setVisible(true);
			});
	}
	
	private void initUI()
	{
		
		//icon
		icon = new ImageIcon("icon.png");
		setIconImage(icon.getImage());
		
		//buttons
		sortButton = new JButton("Sort");		
		sortButton.addActionListener((ActionEvent event) -> {startSort();});
		
		targetButton = new JButton("Target");
		targetButton.addActionListener((ActionEvent event) -> {setTarget();});
		targetButton.setToolTipText("Select the folder containing the files you would like to sort.");
		
		destButton = new JButton("Destination");
		destButton.addActionListener((ActionEvent event) -> {setDest();});
		destButton.setToolTipText("<html> Set the folder you would like the files sorted into.<br> This folder will be the root of the folder tree for sorting. <br> All other folders will be created here. </html>");
		
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((ActionEvent event) -> {cancelSort();});
		
		
		//Selection box
		String[] s ={"Date Modified", "Date Created"};
		sortOption = new JComboBox<>(s);
		sortOption.setToolTipText("<html> Select the parameter you want to use for sorting. <br> It is usually best to use date modified as most OS' will reset the date created whenever a file is moved.</html>");
		
		//progress bar
		progressBar = new JProgressBar();
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		
		createLayout(sortButton, targetButton, destButton, targetText, destText, sortOption, progressBar, cancelButton);

		//window
		setTitle("FileSort");
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	private void createLayout(JComponent... arg)
	{
		Container pane = getContentPane();
		GroupLayout gl = new GroupLayout(pane);
		pane.setLayout(gl);
		
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);
	
		gl.setHorizontalGroup(gl.createParallelGroup(Alignment.LEADING)
									.addGroup(gl.createSequentialGroup()
											.addContainerGap()
												.addGroup(gl.createParallelGroup(Alignment.LEADING)
													.addGroup(gl.createSequentialGroup()
														.addGroup(gl.createParallelGroup(Alignment.LEADING, false )
															.addComponent(arg[1])
															.addComponent(arg[5])
															.addComponent(arg[2]))
														.addGap(18)
														.addGroup(gl.createParallelGroup(Alignment.LEADING)
															.addComponent(arg[3])
															.addComponent(arg[4])))
														.addComponent(arg[6])
														.addGroup(gl.createSequentialGroup()
															.addComponent(arg[0])
															.addPreferredGap(ComponentPlacement.RELATED, 236, Short.MAX_VALUE)
															.addComponent(arg[7])))
														.addContainerGap())
				);

		gl.setVerticalGroup(gl.createParallelGroup()
									.addGroup(gl.createSequentialGroup()
											.addContainerGap()
											.addGroup(gl.createParallelGroup(Alignment.BASELINE)
													.addComponent(arg[1])
													.addComponent(arg[3]))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(gl.createParallelGroup(Alignment.BASELINE)
													.addComponent(arg[2])
													.addComponent(arg[4]))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(arg[5])
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(arg[6])
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(gl.createParallelGroup(Alignment.BASELINE)
													.addComponent(arg[0])
													.addComponent(arg[7]))
											.addContainerGap(102, Short.MAX_VALUE))														
				);
		pack();
	}
	
	private void startSort(){
		sortButton.setEnabled(false);
		task = new Task();
		task.addPropertyChangeListener(this);
		task.execute();
	}

	private void cancelSort()
	{
		task.cancel(true);
		progressBar.setValue(0);
	}
		
	private void setTarget()
	{
		fdia = new JFileChooser();
		fdia.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fdia.setAcceptAllFileFilterUsed(false);
		int returnVal = fdia.showOpenDialog(FileSort.this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			targetText.setText(fdia.getSelectedFile().getPath());
		}		
	}
	
	private void setDest()
	{
		fdia = new JFileChooser();
		fdia.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fdia.setAcceptAllFileFilterUsed(false);
		int returnVal = fdia.showOpenDialog(FileSort.this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			destText.setText(fdia.getSelectedFile().getPath());
		}
	}

	public void propertyChange(PropertyChangeEvent evt)
	{
		if("progress" == evt.getPropertyName())
		{
			progressBar.setValue((Integer)evt.getNewValue());
		}
		if("state" == evt.getPropertyName())
		{
			if(task.isDone())
			{
				sortButton.setEnabled(true);
				progressBar.setValue(0);
			}
		}
	}
	
	class Task extends SwingWorker <Void, Void>
	{
	
		@Override
		protected Void doInBackground() throws Exception {
			setProgress(0);
			sortFiles();
			return null;
		}
		
		@Override
		public void done()
		{
			JOptionPane.showMessageDialog(null, "Sorting complete!");
		}
		
		private Path buildPath(String dest, FileTime time)
		{
			Path path;
			
			path = Paths.get(dest + "\\" + time.toString().substring(0,4));
			
			if(!Files.isDirectory(path))
			{
				try {
					Files.createDirectory(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			switch (time.toString().substring(5, 7))
			{
				case "01":
					path = Paths.get(path.toString().concat("\\January"));
					break;
				case "02":
					path = Paths.get(path.toString().concat("\\February"));
					break;
				case "03":
					path = Paths.get(path.toString().concat("\\March"));
					break;
				case "04":
					path = Paths.get(path.toString().concat("\\April"));
					break;
				case "05":
					path = Paths.get(path.toString().concat("\\May"));
					break;
				case "06":
					path = Paths.get(path.toString().concat("\\June"));
					break;
				case "07":
					path = Paths.get(path.toString().concat("\\July"));
					break;
				case "08":
					path = Paths.get(path.toString().concat("\\August"));
					break;
				case "09":
					path = Paths.get(path.toString().concat("\\September"));
					break;
				case "10":
					path = Paths.get(path.toString().concat("\\October"));
					break;
				case "11":
					path = Paths.get(path.toString().concat("\\November"));
					break;
				case "12":
					path = Paths.get(path.toString().concat("\\December"));
					break;
			}
			
			if(!Files.isDirectory(path))
			{
				try {
					Files.createDirectory(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			path = Paths.get(path.toString().concat("\\" + time.toString().substring(8, 10)));
			
			switch (path.toString().substring(path.toString().length()-1))
			{
				case "1": path = Paths.get(path.toString() + "st");
					break;
				case "2": path = Paths.get(path.toString() + "nd");
					break;
				case "3": path = Paths.get(path.toString() + "rd");
					break;
				default: path = Paths.get(path.toString() + "th");
					break;
			}
			
			if(!Files.isDirectory(path))
			{
				try {
					Files.createDirectory(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
					
			return path;
		
		}
		
		/***
		 * Primary Method for sorting the files and a given location to the destination.
		 * 
		 */
		private void sortFiles()
		{

			File[] files = new File(targetText.getText()).listFiles();
			FileTime time = null;
			Path destPath;

			for(File file : files)
			{
		
				try {
					time = Files.getFileAttributeView(file.toPath(),BasicFileAttributeView.class).readAttributes().lastModifiedTime();	
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				destPath = buildPath(destText.getText(), time);

				try {
					Files.move(file.toPath(), Paths.get(destPath.toString() + "\\" + file.getName()), StandardCopyOption.ATOMIC_MOVE);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				float progress;
				progress = (float)Arrays.asList(files).indexOf(file)/(float)files.length * 100;
				setProgress((int) progress);
			}
		}
	}
}
